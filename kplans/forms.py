from django import forms
from kplans.models import Choice
from kplans.contrib.constants import KPlansK


class ChoiceAdminForm(forms.ModelForm):
    type = forms.ChoiceField(choices=[])

    class Meta:
        model = Choice
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['type'].choices = KPlansK.get_choices_by('CHOICE_')
