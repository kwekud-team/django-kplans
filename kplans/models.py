from django.db import models
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from kplans.contrib.abstract import BaseModel, SiteBaseModel
from kplans.contrib.constants import KPlansK


class Choice(SiteBaseModel):
    code = models.CharField(max_length=100, db_index=True)
    name = models.CharField(max_length=250)
    type = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    order = models.PositiveIntegerField(default=1000)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('order',)
        unique_together = ('site', 'type', 'code',)


class Plan(SiteBaseModel):
    code = models.CharField(max_length=100, db_index=True)
    name = models.CharField(max_length=250)
    description = models.TextField(blank=True)
    is_free = models.BooleanField(default=False)
    is_default = models.BooleanField(default=False)
    is_available = models.BooleanField(default=False, help_text=_('Is still available for purchase'))
    is_visible = models.BooleanField(default=True, db_index=True, help_text=_('Is visible in current offer'))
    is_recommended = models.BooleanField(default=False)
    use_custom_pricing = models.BooleanField(default=False)
    order = models.PositiveIntegerField(default=1000)
    extra = models.JSONField(default=dict, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('order',)
        unique_together = ('site', 'code',)

    def save(self, **kwargs):
        # Ensure only 1 plan is default per site
        if self.is_default:
            Plan.objects.filter(site=self.site).exclude(pk=self.pk).update(is_default=False)
        super().save(**kwargs)


class Pricing(SiteBaseModel):
    name = models.CharField(max_length=250)
    period = models.PositiveIntegerField(default=30)
    order = models.PositiveIntegerField(default=1000)

    def __str__(self):
        return f'{self.name} {self.period} days'

    class Meta:
        ordering = ('period',)
        unique_together = ('site', 'period',)

    def save(self, **kwargs):
        self.name = self.name or f'{self.period} days'
        super().save(**kwargs)


class Feature(SiteBaseModel):
    code = models.CharField(max_length=100, db_index=True)
    name = models.CharField(max_length=250)
    unit = models.CharField(_('unit'), max_length=100, blank=True)
    description = models.TextField(blank=True) 
    category = models.ForeignKey(Choice, null=True, blank=True, on_delete=models.DO_NOTHING,
                                 related_name='feature_category',
                                 limit_choices_to={'type': KPlansK.CHOICE_FEATURE_CATEGORY.value})
    is_boolean = models.BooleanField(_('is boolean'), default=False)
    order = models.PositiveIntegerField(default=1000)
    extra = models.JSONField(default=dict, blank=True)

    def __str__(self):
        return self.code

    class Meta:
        ordering = ('order',)
        unique_together = ('site', 'code',)


class PlanPricing(SiteBaseModel):
    plan = models.ForeignKey('Plan', on_delete=models.CASCADE)
    pricing = models.ForeignKey('Pricing', on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=7, decimal_places=2, db_index=True)
    currency = models.ForeignKey(Choice, on_delete=models.CASCADE, null=True,
                                 limit_choices_to={'type': KPlansK.CHOICE_CURRENCY_CODE.value})
    order = models.PositiveIntegerField(default=1000)
    has_automatic_renewal = models.BooleanField(default=False, help_text=_('Use automatic renewal if possible?'))

    def __str__(self):
        return f'{self.plan.name} {self.pricing}'

    class Meta:
        ordering = ('order', 'pricing__period',)
        unique_together = ('site', 'plan', 'pricing')

    def save(self, *args, **kwargs):
        self.site = self.plan.site
        super().save(*args, **kwargs)


class PlanFeature(SiteBaseModel):
    plan = models.ForeignKey('Plan', on_delete=models.CASCADE)
    feature = models.ForeignKey('Feature', on_delete=models.CASCADE)
    value = models.IntegerField(default=1, null=True, blank=True)
    has_access = models.BooleanField(default=False)
    highlighted = models.BooleanField(default=False)
    note = models.CharField(max_length=250, null=True, blank=True)
    order = models.PositiveIntegerField(default=1000)

    def __str__(self):
        return f'{self.plan}: {self.feature}'

    class Meta:
        ordering = ('order', 'feature__order',)
        unique_together = ('site', 'plan', 'feature')

    def save(self, *args, **kwargs):
        self.site = self.plan.site
        super().save(*args, **kwargs)


class Account(SiteBaseModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    plan = models.ForeignKey(Plan, on_delete=models.CASCADE)
    extended_date = models.DateField(null=True, blank=True)
    status = models.ForeignKey(Choice, on_delete=models.CASCADE, related_name='account_status',
                               limit_choices_to={'type': KPlansK.CHOICE_ACCOUNT_STATUS.value})
    is_valid = models.BooleanField(default=False)
    current_order = models.ForeignKey('kplans.Order', on_delete=models.DO_NOTHING, null=True, blank=True,
                                      related_name='account_current_order')
    balance = models.DecimalField(default=0, max_digits=7, decimal_places=2)

    def __str__(self):
        return f'{self.site}: {self.user}'

    class Meta:
        unique_together = ("site", 'user')


class PayEntry(SiteBaseModel):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    type = models.CharField(max_length=10)
    paid_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    currency = models.ForeignKey(Choice, on_delete=models.CASCADE, related_name='payentry_currency',
                                 limit_choices_to={'type': KPlansK.CHOICE_CURRENCY_CODE.value})
    amount = models.DecimalField(max_digits=7, decimal_places=2)
    calc_amount = models.DecimalField(max_digits=7, decimal_places=2)
    ext_object_pk = models.BigIntegerField()
    ext_content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    ext_content_object = GenericForeignKey(ct_field='ext_content_type', fk_field='ext_object_pk')

    def __str__(self):
        return f'{self.date_created}: {self.amount}'

    class Meta:
        ordering = ('-date_created',)
        unique_together = ['account', 'ext_object_pk', 'ext_content_type']

    def save(self, *args, **kwargs):
        self.site = self.account.site
        super().save(*args, **kwargs)


class Order(SiteBaseModel):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    plan_pricing = models.ForeignKey(PlanPricing, on_delete=models.CASCADE, related_name='order_plan_pricing')
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    expiry_date = models.DateField(null=True, blank=True)
    date_completed = models.DateTimeField(null=True, blank=True)
    date_validated = models.DateTimeField(null=True, blank=True)
    amount = models.DecimalField(max_digits=7, decimal_places=2)
    status = models.ForeignKey(Choice, on_delete=models.CASCADE, related_name='order_status',
                               limit_choices_to={'type': KPlansK.CHOICE_ORDER_STATUS.value})
    currency = models.ForeignKey(Choice, on_delete=models.CASCADE, related_name='order_currency',
                                 limit_choices_to={'type': KPlansK.CHOICE_CURRENCY_CODE.value})
    payment_ref = models.CharField(max_length=100, null=True, blank=True)
    response_code = models.SmallIntegerField(default=-1)
    response_message = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return f'Order #{self.pk}'

    class Meta:
        ordering = ('-date_created',)

    def save(self, *args, **kwargs):
        self.site = self.account.site
        super().save(*args, **kwargs)

    def get_display_name(self):
        return f'{self.plan_pricing.plan.name} / {self.plan_pricing.pricing.name}'
