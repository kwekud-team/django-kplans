from django.urls import path

from kplans import views

app_name = 'kplans'

urlpatterns = [
    path('pay-broker/<int:order_pk>/<str:provider_pk>/', views.PayBrokerView.as_view(), name='pay_broker'),
]
