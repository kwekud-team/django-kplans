import datetime
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.test import TestCase
from django.utils import timezone

from kpay.models import Gateway
from kplans.contrib.utils.account import AccountUtils
from kplans.contrib.utils.plan import PlanUtils
from kplans.contrib.constants import KPlansK

User = get_user_model()


class AccountTestCase(TestCase):

    def setUp(self):
        self.site = Site.objects.get_current()
        self.user = User.objects.create_user('test', password='1234')
        self.plan_utils = PlanUtils(self.site)
        self.account_utils = AccountUtils(self.site)
        self.currency_code = 'USD'
        self.gateway = Gateway.objects.create(site=self.site, identifier='default', name='Default')

    def _quick_plan_pricing(self, code, period, price, currency_code):
        plan = self.plan_utils.get_plan(code, is_free=price == 0)
        pricing = self.plan_utils.get_pricing(period)
        return self.plan_utils.get_plan_pricing(plan, pricing, price, currency_code)

    def test_init_new_account(self):
        # Test new account
        default_plan = self.plan_utils.get_default_plan()
        account = self.account_utils.init_account(self.user, default_plan)

        self.assertEqual(account.status.code, KPlansK.STATUS_ACTIVE.value)
        self.assertFalse(account.is_valid)
        self.assertEqual(account.balance, 0)
        self.assertEqual(account.order_set.count(), 0)
        self.assertEqual(account.payentry_set.count(), 0)

        # Test Idempotency
        account = self.account_utils.init_account(self.user, default_plan)
        self.assertEqual(account.status.code, KPlansK.STATUS_ACTIVE.value)
        self.assertFalse(account.is_valid)
        self.assertEqual(account.balance, 0)
        self.assertEqual(account.order_set.count(), 0)
        self.assertEqual(account.payentry_set.count(), 0)

    def test_create_account_order(self):
        default_plan = self.plan_utils.get_default_plan()
        account = self.account_utils.init_account(self.user, default_plan)
        plan_pricing = self._quick_plan_pricing('basic', period=30, price=100, currency_code=self.currency_code)
        order = self.account_utils.create_order(account, self.user, plan_pricing)

        self.assertEqual(order.status.code, 'new')
        self.assertEqual(order.amount, 100)
        self.assertIsNone(order.date_completed)
        self.assertIsNone(order.date_validated)
        self.assertIsNone(order.expiry_date)
        self.assertEqual(order.payment_ref, '')
        self.assertEqual(order.response_code, -1)
        self.assertEqual(order.response_message, '')

    def test_manipulate_balance(self):
        default_plan = self.plan_utils.get_default_plan()
        account = self.account_utils.init_account(self.user, default_plan)
        plan_pricing = self._quick_plan_pricing('Basic', period=30, price=100, currency_code=self.currency_code)
        order = self.account_utils.create_order(account, self.user, plan_pricing)
        payment = self.account_utils.prepare_payment(order, self.gateway)
        payment_ref = str(order.guid)

        # Credit: +60 | Balance: 60
        pay_entry = self.account_utils.credit_account(account, self.user, self.currency_code, 60, payment)
        self.assertEqual(pay_entry.currency.code, self.currency_code)
        self.assertEqual(pay_entry.amount, 60)
        self.assertEqual(pay_entry.calc_amount, 60)
        self.assertEqual(pay_entry.ext_content_object, payment)
        self.assertEqual(pay_entry.type, 'credit')
        self.assertEqual(pay_entry.paid_by, self.user)
        self.assertEqual(payment.source_ref, payment_ref)
        self.assertEqual(account.balance, 60)

        # Credit: +20 | Balance: 80
        payment = self.account_utils.prepare_payment(order, self.gateway)
        pay_entry = self.account_utils.credit_account(account, self.user, self.currency_code, 20, payment)
        self.assertEqual(pay_entry.currency.code, self.currency_code)
        self.assertEqual(pay_entry.amount, 20)
        self.assertEqual(pay_entry.calc_amount, 20)
        self.assertEqual(pay_entry.ext_content_object, payment)
        self.assertEqual(pay_entry.paid_by, self.user)
        self.assertEqual(payment.source_ref, f'{payment_ref}#1')
        self.assertEqual(account.balance, 80)

        # debit: -30 | Balance: 50
        payment = self.account_utils.prepare_payment(order, self.gateway)
        pay_entry = self.account_utils.debit_account(account, self.user, self.currency_code, 30, payment)
        self.assertEqual(pay_entry.currency.code, self.currency_code)
        self.assertEqual(pay_entry.amount, 30)
        self.assertEqual(pay_entry.calc_amount, -30)
        self.assertEqual(pay_entry.ext_content_object, payment)
        self.assertEqual(pay_entry.paid_by, self.user)
        self.assertEqual(payment.source_ref, f'{payment_ref}#2')
        self.assertEqual(account.balance, 50)

        # debit: -60 | Balance: 50 : Can't debit if balance is insufficient
        payment = self.account_utils.prepare_payment(order, self.gateway)
        pay_entry = self.account_utils.debit_account(account, self.user, self.currency_code, 60, payment)
        self.assertIsNone(pay_entry)
        self.assertEqual(payment.source_ref, f'{payment_ref}#3')
        self.assertEqual(account.balance, 50)

    def test_order_payment(self):
        default_plan = self.plan_utils.get_default_plan()
        account = self.account_utils.init_account(self.user, default_plan)
        plan_pricing = self._quick_plan_pricing('Basic', period=30, price=100, currency_code=self.currency_code)
        order = self.account_utils.create_order(account, self.user, plan_pricing)
        payment = self.account_utils.prepare_payment(order, self.gateway)
        expected_expiry_date = (timezone.now() + datetime.timedelta(days=30)).date()

        # Credit: +60 | Balance: 60
        self.account_utils.credit_account(account, self.user, self.currency_code, 100, payment)
        pay_entry = self.account_utils.pay_for_order(order, self.user)

        self.assertEqual(order.status.code, 'paid')
        self.assertIsNotNone(order.date_completed)
        self.assertIsNotNone(order.date_validated)
        self.assertIsNotNone(order.expiry_date)
        self.assertEqual(order.expiry_date, expected_expiry_date)
        self.assertEqual(order.response_code, 0)
        self.assertEqual(order.response_message, 'Ok')
        self.assertEqual(account.balance, 0)
        self.assertEqual(account.status.code, KPlansK.STATUS_ACTIVE.value)
        self.assertEqual(account.plan, plan_pricing.plan)
        self.assertEqual(account.current_order, order)

    def test_order_payment_not_enough_funds(self):
        default_plan = self.plan_utils.get_default_plan()
        account = self.account_utils.init_account(self.user, default_plan)
        plan_pricing = self._quick_plan_pricing('Basic', period=30, price=100, currency_code=self.currency_code)
        order = self.account_utils.create_order(account, self.user, plan_pricing)
        payment = self.account_utils.prepare_payment(order, self.gateway)

        # Credit: +60 | Balance: 60
        self.account_utils.credit_account(account, self.user, self.currency_code, 90, payment)
        pay_entry = self.account_utils.pay_for_order(order, self.user)

        self.assertEqual(order.status.code, KPlansK.STATUS_INVALID.value)
        self.assertIsNone(order.date_completed)
        self.assertIsNone(order.date_validated)
        self.assertIsNone(order.expiry_date)
        self.assertEqual(order.response_code, 1)
        self.assertIn('Insufficient Funds', order.response_message)
        self.assertEqual(account.balance, 90)
        self.assertEqual(account.status.code, KPlansK.STATUS_ACTIVE.value)
        self.assertEqual(account.plan, default_plan)

    def test_order_payment_balance_over(self):
        default_plan = self.plan_utils.get_default_plan()
        account = self.account_utils.init_account(self.user, default_plan)
        plan_pricing = self._quick_plan_pricing('Basic', period=30, price=100, currency_code=self.currency_code)
        order = self.account_utils.create_order(account, self.user, plan_pricing)
        payment = self.account_utils.prepare_payment(order, self.gateway)

        self.account_utils.credit_account(account, self.user, self.currency_code, 110, payment)
        self.account_utils.pay_for_order(order, self.user)

        self.assertEqual(account.balance, 10)

        self.assertEqual(account.status.code, KPlansK.STATUS_ACTIVE.value)
        self.assertEqual(account.plan, plan_pricing.plan)

    def test_change_plan(self):
        default_plan = self.plan_utils.get_default_plan()
        account = self.account_utils.init_account(self.user, default_plan)

        # Plan 1
        plan_pricing1 = self._quick_plan_pricing('Basic', period=30, price=100, currency_code=self.currency_code)
        order1 = self.account_utils.create_order(account, self.user, plan_pricing1)
        payment1 = self.account_utils.prepare_payment(order1, self.gateway)
        self.account_utils.credit_account(account, self.user, self.currency_code, 100, payment1)
        self.account_utils.pay_for_order(order1, self.user)

        self.assertEqual(account.balance, 0)
        self.assertEqual(account.status.code, KPlansK.STATUS_ACTIVE.value)
        self.assertEqual(account.plan, plan_pricing1.plan)

        # Plan 2
        plan_pricing2 = self._quick_plan_pricing('Pro', period=30, price=200, currency_code=self.currency_code)
        order2 = self.account_utils.create_order(account, self.user, plan_pricing2)
        payment2 = self.account_utils.prepare_payment(order2, self.gateway)
        self.account_utils.credit_account(account, self.user, self.currency_code, 200, payment2)
        self.account_utils.pay_for_order(order2, self.user)

        self.assertEqual(account.balance, 0)
        self.assertEqual(account.status.code, KPlansK.STATUS_ACTIVE.value)
        self.assertEqual(account.plan, plan_pricing2.plan)


    # def test_free_account_no_order(self):
    #     status_new = get_choice(self.site, 'account-status', code='new')
    #     plan_pricing = self.plan_utils.quick_plan_pricing('one', period=30, price=0, currency_code=self.currency_code)
    #
    #     account = self.account_utils.new_account(self.user, plan_pricing.plan, plan_pricing=plan_pricing)
    #     self.assertEqual(account.status, status_new)
    #     self.assertFalse(account.is_valid)
    #     self.assertEqual(account.order_set.count(), 0)
    #
    # def test_get_user_account(self):
    #     plan_pricing = self.plan_utils.quick_plan_pricing('one', period=30, price=10, currency_code=self.currency_code)
    #     new_account = self.account_utils.new_account(self.user, plan_pricing.plan, plan_pricing=plan_pricing)
    #     account = self.account_utils.get_user_account(self.user)
    #     order = self.account_utils.get_user_account_order(self.user)
    #
    #     self.assertEqual(new_account, account)
    #     self.assertEqual(account.current_order, order)
    #
    # def test_activate_account_valid(self):
    #     plan_pricing = self.plan_utils.quick_plan_pricing('one', period=30, price=100, currency_code=self.currency_code)
    #     account = self.account_utils.new_account(self.user, plan_pricing.plan, plan_pricing=plan_pricing)
    #     pay_ref = 'PayRef#1'
    #     expected_expiry_date = (timezone.now() + datetime.timedelta(days=30)).date()
    #
    #     order = account.current_order
    #     self.account_utils.activate_account_order(order, 100, pay_ref)
    #
    #     self.assertEqual(order.status.code, 'completed')
    #     self.assertIsNotNone(order.date_completed)
    #     self.assertIsNotNone(order.date_validated)
    #     self.assertIsNotNone(order.expiry_date)
    #     self.assertEqual(order.expiry_date, expected_expiry_date)
    #     self.assertEqual(order.payment_ref, pay_ref)
    #     self.assertEqual(order.response_code, 0)
    #     self.assertEqual(order.response_message, 'Ok')
    #
    #     self.assertEqual(account.status.code, 'paid')
    #
    # def test_activate_account_invalid(self):
    #     plan_pricing = self.plan_utils.quick_plan_pricing('one', period=30, price=100, currency_code=self.currency_code)
    #     account = self.account_utils.new_account(self.user, plan_pricing.plan, plan_pricing=plan_pricing)
    #     pay_ref = 'PayRef#001'
    #     expected_expiry_date = (timezone.now() + datetime.timedelta(days=30)).date()
    #
    #     order = account.current_order
    #     self.account_utils.activate_account_order(order, 90, pay_ref)
    #
    #     self.assertEqual(order.status.code, 'invalid')
    #     self.assertIsNone(order.date_completed)
    #     self.assertIsNone(order.date_validated)
    #     self.assertIsNone(order.expiry_date)
    #     self.assertEqual(order.payment_ref, pay_ref)
    #     self.assertEqual(order.response_code, 1)
    #     self.assertIn('Insufficient Funds', order.response_message)
    #
    #     self.assertEqual(account.status.code, 'invalid')
    #
    # def test_extend_account(self):
    #     plan_pricing = self.plan_utils.quick_plan_pricing('one', period=30, price=100, currency_code=self.currency_code)
    #     expected_expiry_date = (timezone.now() + datetime.timedelta(days=60)).date()
    #
    #     new_account = self.account_utils.new_account(self.user, plan_pricing.plan, plan_pricing=plan_pricing)
    #     self.account_utils.activate_account_order(new_account.current_order, 100, 'PayRef#1')
    #     extend_order = self.account_utils.extend_account(new_account, plan_pricing, self.user, 100, 'PayRef#2')
    #
    #     self.assertEqual(extend_order.status.code, 'completed')
    #     self.assertIsNotNone(extend_order.date_completed)
    #     self.assertIsNotNone(extend_order.date_validated)
    #     self.assertIsNotNone(extend_order.expiry_date)
    #     self.assertEqual(extend_order.expiry_date, expected_expiry_date)
    #     self.assertEqual(extend_order.payment_ref, 'PayRef#2')
    #     self.assertEqual(extend_order.response_code, 0)
    #     self.assertEqual('Ok', extend_order.response_message)
    #
    #     self.assertEqual(new_account.status.code, 'paid')
    #     self.assertEqual(new_account.extended_date, extend_order.date_completed)
    #
    # def test_change_account(self):
    #     plan_pricing_1 = self.plan_utils.quick_plan_pricing('one', period=30, price=100, currency_code=self.currency_code)
    #     plan_pricing_2 = self.plan_utils.quick_plan_pricing('two', period=30, price=200, currency_code=self.currency_code)
    #     expected_expiry_date = (timezone.now() + datetime.timedelta(days=30)).date()
    #
    #     account = self.account_utils.new_account(self.user, plan_pricing_1.plan, plan_pricing=plan_pricing_1)
    #     self.account_utils.activate_account_order(account.current_order, 100, 'PayRef#1')
    #     order = self.account_utils.change_account(account, plan_pricing_2, self.user, 200, 'PayRef#2')
    #
    #     self.assertEqual(order.status.code, 'completed')
    #     self.assertEqual(order.plan_pricing, plan_pricing_2)
    #     self.assertIsNotNone(order.date_validated)
    #     self.assertIsNotNone(order.expiry_date)
    #     self.assertEqual(order.expiry_date, expected_expiry_date)
    #
    #     self.assertEqual(account.plan, plan_pricing_2.plan)
    #     self.assertEqual(account.status.code, 'paid')
    #
    # def test_suspend_account_non_payment(self):
    #     plan_pricing = self.plan_utils.quick_plan_pricing('one', period=30, price=100, currency_code=self.currency_code)
    #     future_date = (timezone.now() + datetime.timedelta(days=31)).date()
    #
    #     account = self.account_utils.new_account(self.user, plan_pricing.plan, plan_pricing=plan_pricing)
    #     self.account_utils.activate_account_order(account.current_order, 100, 'PayRef#1')
    #     self.account_utils.suspend_expired_accounts(future_date)
    #
    #     account.refresh_from_db()
    #     self.assertEqual(account.status.code, 'expired')
    #     self.assertEqual(account.current_order.status.code, 'expired')
    #
    # def test_cancel_account_order(self):
    #     plan_pricing = self.plan_utils.quick_plan_pricing('one', period=30, price=100, currency_code=self.currency_code)
    #     account = self.account_utils.new_account(self.user, plan_pricing.plan, plan_pricing=plan_pricing)
    #
    #     self.account_utils.cancel_account_order(account.current_order, 'PayRef#1', 'Canceled payment')
    #
    #     account.refresh_from_db()
    #     order = account.current_order
    #     self.assertEqual(order.status.code, 'canceled')
    #     self.assertEqual(order.payment_ref, 'PayRef#1')
    #     self.assertIsNone(order.date_validated)
    #     self.assertIsNotNone(order.date_completed)
    #     self.assertEqual(order.response_code, 2)
    #     self.assertEqual(order.response_message, 'Canceled payment')
    #
    #
