from django.urls import reverse
from django.views.generic import View
from django.shortcuts import redirect, get_object_or_404
from kommons.utils.http import get_client_ip

from kpay.models import Gateway
from kpay.contrib.utils.pay import PayUtils
from kpay.contrib.dclasses import PaymentItem
from kplans.models import Order


class PayBrokerView(View):

    def get(self, request, *args, **kwargs):
        order = get_object_or_404(Order, pk=kwargs['order_pk'])
        gateway = get_object_or_404(Gateway, pk=kwargs['provider_pk'])

        payment = PayUtils(order.site).init_payment(gateway, self._get_payment_item(order))

        url = reverse('kpay:payment_form', args=(payment.pk,))
        return redirect(url)

    def _get_payment_item(self, order):
        return PaymentItem(
            reference=str(order.guid),
            description=order.get_display_name(),
            amount=order.amount,
            currency=order.currency.code,
            ip_address=get_client_ip(self.request)
        )

    # def update_order(self, order, payment):
    #     status = get_choice(order.plan_pricing.site, KPlansK.CHOICE_ORDER_STATUS.value,
    #                         code=KPlansK.STATUS_PAYMENT_INITIATED.value)
    #     Order.objects.filter(pk=order.pk).update(payment_ref=payment.guid, status=status)


    # return Payment.objects.create(
    #     variant='default',
    #     description='Book purchase',
    #     total=Decimal(120),
    #     tax=Decimal(20),
    #     currency='USD',
    #     delivery=Decimal(10),
    #     billing_first_name='Sherlock',
    #     billing_last_name='Holmes',
    #     billing_address_1='221B Baker Street',
    #     billing_address_2='',
    #     billing_city='London',
    #     billing_postcode='NW1 6XE',
    #     billing_country_code='GB',
    #     billing_country_area='Greater London',
    #     customer_ip_address='127.0.0.1'
    # )
