from enum import Enum


class KPlansK(Enum):
    CHOICE_CURRENCY_CODE = 'currency'
    CHOICE_FEATURE_CATEGORY = 'feature-category'
    CHOICE_ACCOUNT_STATUS = 'subscription-status'
    CHOICE_ORDER_STATUS = 'order-status'

    PAY_CREDIT = 'credit'
    PAY_DEBIT = 'debit'

    STATUS_NEW = 'new'
    STATUS_PAYMENT_INITIATED = 'payment initiated'
    STATUS_ACTIVE = 'active'
    STATUS_PAID = 'paid'
    STATUS_EXPIRED = 'expired'
    STATUS_SUSPENDED = 'suspended'
    STATUS_COMPLETED = 'completed'
    STATUS_INVALID = 'invalid'
    STATUS_CANCELED = 'canceled'
    STATUS_RETURNED = 'returned'

    RESP_OK__0 = 'Ok'
    RESP_INSUFFICIENT_FUNDS__1 = 'Insufficient Funds'
    RESP_CANCELED__2 = 'Canceled by payment provider'

    @classmethod
    def get_choices_by(cls, prefix):
        xs = []
        for name, member in cls.__members__.items():
            if name.startswith(prefix):
                label = member.value.replace('-', ' ').title()
                xs.append((member.value, label))
        return tuple(xs)

    def get_response_code(self):
        return int(self.name.split('__')[-1])
