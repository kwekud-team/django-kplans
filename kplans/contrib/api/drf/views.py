from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action

from kplans.models import Choice, Plan, Pricing, Feature, PlanPricing, PlanFeature
from kplans.contrib.api.drf.serializers import (ChoiceSerializer, PlanSerializer, FeatureSerializer, PricingSerializer,
                                                PlanFeatureSerializer, PlanPricingSerializer)
from kplans.contrib.api.drf.utils import PlanPricingTable


class ChoiceViewSet(viewsets.ModelViewSet):
    queryset = Choice.objects.all()
    serializer_class = ChoiceSerializer


class PlanViewSet(viewsets.ModelViewSet):
    queryset = Plan.objects.all()
    serializer_class = PlanSerializer

    @action(methods=['get'], detail=False, url_path='fn-pricing-table')
    def get_pricing_table(self, request, pk=None):
        res = PlanPricingTable(request).get_pricing_table()
        return Response(**res)


class PricingViewSet(viewsets.ModelViewSet):
    queryset = Pricing.objects.all()
    serializer_class = PricingSerializer


class FeatureViewSet(viewsets.ModelViewSet):
    queryset = Feature.objects.all()
    serializer_class = FeatureSerializer


class PlanPricingViewSet(viewsets.ModelViewSet):
    queryset = PlanPricing.objects.all()
    serializer_class = PlanPricingSerializer


class PlanFeatureViewSet(viewsets.ModelViewSet):
    queryset = PlanFeature.objects.all()
    serializer_class = PlanFeatureSerializer
