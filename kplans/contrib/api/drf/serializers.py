from rest_framework import serializers
from rest_flex_fields import FlexFieldsModelSerializer

from kplans.models import Choice, Plan, Pricing, Feature, PlanPricing, PlanFeature
from kplans.contrib.attrs import ChoiceAttr, PlanAttr, PricingAttr, FeatureAttr, PlanPricingAttr, PlanFeatureAttr


class BaseSerializer(FlexFieldsModelSerializer, serializers.HyperlinkedModelSerializer):
    pass


class ChoiceSerializer(BaseSerializer):
    site = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Choice
        fields = ChoiceAttr.drf_serializer_fields


class PlanSerializer(BaseSerializer):
    site = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Plan
        fields = PlanAttr.drf_serializer_fields


class PricingSerializer(BaseSerializer):
    site = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Pricing
        fields = PricingAttr.drf_serializer_fields


class FeatureSerializer(BaseSerializer):
    site = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Feature
        fields = FeatureAttr.drf_serializer_fields
        expandable_fields = {
            'category': ChoiceSerializer
        }


class PlanPricingSerializer(BaseSerializer):
    site = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = PlanPricing
        fields = PlanPricingAttr.drf_serializer_fields
        expandable_fields = {
            'pricing': PricingSerializer
        }


class PlanFeatureSerializer(BaseSerializer):
    site = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = PlanFeature
        fields = PlanFeatureAttr.drf_serializer_fields
        expandable_fields = {
            'feature': FeatureSerializer
        }
