from django.urls import path, include

from rest_framework import routers
from kplans.contrib.api.drf.views import (ChoiceViewSet, PlanViewSet, FeatureViewSet, PricingViewSet,
                                          PlanFeatureViewSet, PlanPricingViewSet)


router = routers.DefaultRouter()
router.register('kplans/choice', ChoiceViewSet)
router.register('kplans/plan', PlanViewSet)
router.register('kplans/feature', FeatureViewSet)
router.register('kplans/pricing', PricingViewSet)
router.register('kplans/planfeature', PlanFeatureViewSet)
router.register('kplans/planpricing', PlanPricingViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
