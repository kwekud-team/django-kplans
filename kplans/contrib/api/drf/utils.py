from django.contrib.sites.models import Site
from rest_framework import status as drf_status

from kplans.models import Plan, Feature, PlanFeature
from kplans.contrib.api.drf.serializers import (PlanSerializer, FeatureSerializer, PlanPricingSerializer,
                                                PlanFeatureSerializer)


class PlanPricingTable:

    def __init__(self, request):
        self.request = request
        self.domain = request.GET.get('domain')
        self.serializer_context = {'request': self.request}

    def get_site(self):
        return Site.objects.filter(domain__iendswith=str(self.domain)).first()

    def get_pricing_table(self):
        site = self.get_site()

        if site:
            status_code = drf_status.HTTP_200_OK
            data = {}

            plan_xs = []
            fields = ['pk', 'code', 'name', 'description', 'is_free', 'is_recommended', 'use_custom_pricing']
            for plan in Plan.objects.active():
                plan_xs.append({
                    'instance': PlanSerializer(plan, context=self.serializer_context, fields=fields).data,
                    'pricings': self._get_plan_pricings(plan),
                    'features': self._get_plan_features(plan)
                })

            data['plans'] = plan_xs
            data['features'] = self.get_features()
        else:
            data = {'site': f'Site with this domain ({self.domain}) not found'}
            status_code = drf_status.HTTP_404_NOT_FOUND

        return {'data': data, 'status': status_code}

    def _get_plan_pricings(self, plan):
        xs = []
        fields = ['pk', 'price', 'pricing.name', 'pricing.period']
        for obj in plan.planpricing_set.active():
            xs.append({
                'instance': PlanPricingSerializer(obj, context=self.serializer_context, fields=fields,
                                                  expand=['pricing']).data
            })

        return xs

    def _get_plan_features(self, plan, limit=10):
        xs = []
        fields = ['pk', 'feature.name', 'feature.description', 'feature.category.name']
        for obj in plan.planfeature_set.active()[:limit]:
            xs.append({
                'instance': PlanFeatureSerializer(obj, context=self.serializer_context, fields=fields,
                                                  expand=['feature', 'feature.category']).data
            })

        return xs

    def get_features(self):
        xs = []
        plans = Plan.objects.active()
        fields = ['name', 'description']
        for feature in Feature.objects.active():
            tmp = []
            for plan in plans:
                plan_feature = PlanFeature.objects.filter(feature=feature, plan=plan).first()
                if plan_feature:
                    tmp.append(
                        PlanFeatureSerializer(plan_feature, context=self.serializer_context,
                                              fields=['pk', 'has_access', 'note']).data
                    )
                else:
                    tmp.append(None)

            xs.append({
                'instance': FeatureSerializer(feature, context=self.serializer_context, fields=fields).data,
                'plans': tmp
            })

        return xs
