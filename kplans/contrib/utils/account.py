import datetime
from django.utils import timezone
from django.contrib.contenttypes.models import ContentType

from kpay.contrib.dclasses import PaymentItem
from kpay.contrib.utils.pay import PayUtils
from kplans.models import Account, Order, PayEntry
from kplans.contrib.constants import KPlansK
from kplans.contrib.utils.generic import get_choice


class AccountUtils:

    def __init__(self, site):
        self.site = site
        self.pay_utils = PayUtils(self.site)

    def init_account(self, user, plan):
        status = get_choice(self.site, KPlansK.CHOICE_ACCOUNT_STATUS.value, code=KPlansK.STATUS_ACTIVE.value)

        account = Account.objects.update_or_create(
            site=self.site,
            user=user,
            defaults={
                'plan': plan,
                'status': status
            })[0]

        return account

    def create_order(self, account, user, plan_pricing):
        status = get_choice(self.site, KPlansK.CHOICE_ORDER_STATUS.value, code=KPlansK.STATUS_NEW.value)

        return Order.objects.update_or_create(
            site=self.site,
            account=account,
            date_completed__isnull=True,
            defaults={
                'created_by': user,
                'plan_pricing': plan_pricing,
                'amount': plan_pricing.price,
                'currency': plan_pricing.currency,
                'status': status,
                'payment_ref': '',
                'response_message': ''
            })[0]

    def prepare_payment(self, order, gateway):
        payment_item = PaymentItem(
            reference=str(order.guid),
            description=order.get_display_name(),
            amount=order.amount,
            currency=order.currency.code,
        )
        return self.pay_utils.init_payment(gateway, payment_item)

    def pay_for_order(self, order, user):
        pay_entry = self.debit_account(order.account, user, order.currency.code, order.amount, order)
        if pay_entry:
            values = self._success_order(order)
        else:
            values = self._failed_order(order)

        self._update_object(order.account, values['account'])
        self._update_object(order, values['order'])

        return pay_entry

    def _success_order(self, order):
        date_completed = timezone.now()
        expiry_date = date_completed + datetime.timedelta(days=order.plan_pricing.pricing.period)

        return {
            'order': {
                'status': get_choice(self.site, KPlansK.CHOICE_ORDER_STATUS.value, code=KPlansK.STATUS_PAID.value),
                'expiry_date': expiry_date.date(),
                'date_completed': date_completed,
                'date_validated': date_completed,
                'response_code': KPlansK.RESP_OK__0.get_response_code(),
                'response_message': KPlansK.RESP_OK__0.value
            },
            'account': {
                'current_order': order,
                'plan': order.plan_pricing.plan,
                'status': get_choice(self.site, KPlansK.CHOICE_ACCOUNT_STATUS.value, code=KPlansK.STATUS_ACTIVE.value),
            }
        }

    def _failed_order(self, order):
        amount = order.account.balance
        return {
            'order': {
                'status': get_choice(self.site, KPlansK.CHOICE_ORDER_STATUS.value, code=KPlansK.STATUS_INVALID.value),
                'date_completed': None,
                'date_validated': None,
                'response_code': KPlansK.RESP_INSUFFICIENT_FUNDS__1.get_response_code(),
                'response_message': f'{KPlansK.RESP_INSUFFICIENT_FUNDS__1.value}: Expected {order.amount}, got {amount}',
            },
            'account': {

            }
        }

    def get_user_account(self, user, default_plan_pricing=None):
        account = Account.objects.filter(site=self.site, user=user).first()
        if not account and default_plan_pricing:
            account = self.init_account(user, default_plan_pricing)
        return account

    # def get_user_account_order(self, user):
    #     return Order.objects.filter(site=self.site, account__user=user).order_by('-date_created').first()
    #
    # def get_days_remaining(self, order):
    #     if order.expiry_date:
    #         date_interval = order.expiry_date - timezone.now().date()
    #         return date_interval.days

    def _add_pay_entry(self, ttype, account, user, currency_code, amount, ext_object):
        direction = (1 if ttype == KPlansK.PAY_CREDIT.value else -1)
        calc_amount = direction * amount

        content_type = ContentType.objects.get_for_model(ext_object)
        currency = get_choice(self.site, KPlansK.CHOICE_CURRENCY_CODE.value, code=currency_code)

        pay_entry, created = PayEntry.objects.get_or_create(
            site=account.site,
            account=account,
            ext_object_pk=ext_object.pk,
            ext_content_type=content_type,
            defaults={
                'amount': amount,
                'currency': currency,
                'paid_by': user,
                'type': ttype,
                'calc_amount': calc_amount,
            })

        if created:
            account.balance += calc_amount
            account.save(update_fields=['balance'])

        return pay_entry

    def credit_account(self, account, user, currency, amount, ext_object):
        return self._add_pay_entry(KPlansK.PAY_CREDIT.value, account, user, currency, amount, ext_object)

    def debit_account(self, account, user, currency, amount, ext_object):
        # This app does not support negative balance. We don't want to turn into an accounting library
        if account.balance >= amount:
            return self._add_pay_entry(KPlansK.PAY_DEBIT.value, account, user, currency, amount, ext_object)


    def _update_object(self, obj, values):
        for k, v in values.items():
            setattr(obj, k, v)
        obj.save(update_fields=values.keys())

    # def verify_account_payment(self, order, amount, payment_ref):
    #     if amount >= order.amount:
    #         is_paid = True
    #         date_completed = timezone.now()
    #         date_validated = date_completed
    #         response_code = KPlansK.RESP_OK__0.get_response_code()
    #         response_message = KPlansK.RESP_OK__0.value
    #     else:
    #         is_paid = False
    #         date_completed = None
    #         date_validated = None
    #         response_message = f'{KPlansK.RESP_INSUFFICIENT_FUNDS__1.value}: Expected {order.amount}, got {amount}'
    #         response_code = KPlansK.RESP_INSUFFICIENT_FUNDS__1.get_response_code()
    #
    #     return {
    #         'is_paid': is_paid,
    #         'options': {
    #             'date_completed': date_completed,
    #             'date_validated': date_validated,
    #             'payment_ref': payment_ref,
    #             'response_code': response_code,
    #             'response_message': response_message
    #         }
    #     }
    #
    # def activate_account_order(self, order, amount, payment_ref):
    #     res = self.verify_account_payment(order, amount, payment_ref)
    #     values = {'account': {}, 'order': res['options']}
    #
    #     if res['is_paid']:
    #         sub_status = get_choice(self.site, KPlansK.CHOICE_ACCOUNT_STATUS.value, code=KPlansK.STATUS_PAID.value)
    #         values['account']['status'] = sub_status
    #
    #         order_status = get_choice(self.site, KPlansK.CHOICE_ORDER_STATUS.value, code=KPlansK.STATUS_COMPLETED.value)
    #         expiry_date = res['options']['date_completed'] + datetime.timedelta(days=order.plan_pricing.pricing.period)
    #         values['order']['expiry_date'] = expiry_date.date()
    #     else:
    #         order_status = get_choice(self.site, KPlansK.CHOICE_ORDER_STATUS.value, code=KPlansK.STATUS_INVALID.value)
    #         sub_status = get_choice(self.site, KPlansK.CHOICE_ACCOUNT_STATUS.value, code=KPlansK.STATUS_INVALID.value)
    #         values['account']['status'] = sub_status
    #
    #     self._update_object(order.account, values['account'])
    #     self._update_object(order, dict(status=order_status, **values['order']))
    #
    # def extend_account(self, account, plan_pricing, user, amount, payment_ref):
    #     extend_order = self.create_order(account, user, plan_pricing)
    #
    #     res = self.verify_account_payment(extend_order, amount, payment_ref)
    #     values = {'account': {}, 'order': res['options']}
    #     if res['is_paid']:
    #         sub_status = get_choice(self.site, KPlansK.CHOICE_ACCOUNT_STATUS.value, code=KPlansK.STATUS_PAID.value)
    #         values['account']['status'] = sub_status
    #         values['account']['extended_date'] = values['order']['date_completed']
    #
    #         order_status = get_choice(self.site, KPlansK.CHOICE_ORDER_STATUS.value, code=KPlansK.STATUS_COMPLETED.value)
    #         days_remaining = self.get_days_remaining(account.current_order)
    #         days = days_remaining + extend_order.plan_pricing.pricing.period
    #
    #         expiry_date = values['order']['date_completed'] + datetime.timedelta(days=days)
    #         values['order']['expiry_date'] = expiry_date.date()
    #     else:
    #         order_status = get_choice(self.site, KPlansK.CHOICE_ORDER_STATUS.value, code=KPlansK.STATUS_INVALID.value)
    #
    #     self._update_object(extend_order.account, values['account'])
    #     self._update_object(extend_order, dict(status=order_status, **values['order']))
    #
    #     return extend_order
    #
    # def change_account(self, account, plan_pricing, user, amount, payment_ref):
    #     order = self.create_order(account, user, plan_pricing)
    #
    #     res = self.verify_account_payment(order, amount, payment_ref)
    #     values = {'account': {}, 'order': res['options']}
    #     if res['is_paid']:
    #         sub_status = get_choice(self.site, KPlansK.CHOICE_ACCOUNT_STATUS.value, code=KPlansK.STATUS_PAID.value)
    #         values['account']['status'] = sub_status
    #         values['account']['plan'] = plan_pricing.plan
    #
    #         order_status = get_choice(self.site, KPlansK.CHOICE_ORDER_STATUS.value, code=KPlansK.STATUS_COMPLETED.value)
    #         expiry_date = res['options']['date_completed'] + datetime.timedelta(days=order.plan_pricing.pricing.period)
    #         values['order']['expiry_date'] = expiry_date.date()
    #     else:
    #         order_status = get_choice(self.site, KPlansK.CHOICE_ORDER_STATUS.value, code=KPlansK.STATUS_INVALID.value)
    #
    #     self._update_object(order.account, values['account'])
    #     self._update_object(order, dict(status=order_status, **values['order']))
    #
    #     return order

    def cancel_order(self, order, payment_ref, reason):
        order_status = get_choice(self.site, KPlansK.CHOICE_ORDER_STATUS.value, code=KPlansK.STATUS_CANCELED.value)
        values = {
            'date_completed': timezone.now(),
            'response_code': KPlansK.RESP_CANCELED__2.get_response_code(),
            'response_message': reason,
            'payment_ref': payment_ref
        }
        self._update_object(order, dict(status=order_status, **values))

        return order

    # def suspend_expired_accounts(self, check_date):
    #     # Update account
    #     qs = Account.objects.filter(status__code=KPlansK.STATUS_PAID.value, current_order__expiry_date__lt=check_date)
    #     status = get_choice(self.site, KPlansK.CHOICE_ACCOUNT_STATUS.value, code=KPlansK.STATUS_EXPIRED.value)
    #     qs.update(status=status)
    #
    #     # Update order
    #     qs = Order.objects.filter(status__code=KPlansK.STATUS_COMPLETED.value, expiry_date__lt=check_date)
    #     status = get_choice(self.site, KPlansK.CHOICE_ORDER_STATUS.value, code=KPlansK.STATUS_EXPIRED.value)
    #     qs.update(status=status)
