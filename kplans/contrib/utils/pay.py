from django.contrib.contenttypes.models import ContentType
from kplans.models import PayEntry
from kplans.contrib.constants import KPlansK
from kplans.contrib.utils.generic import get_choice


class PayUtils_:

    def __init__(self, site):
        self.site = site

    def _add_pay_entry(self, ttype, account, user, currency_code, amount, ext_object):
        direction = (1 if ttype == KPlansK.PAY_CREDIT.value else -1)
        calc_amount = direction * amount

        content_type = ContentType.objects.get_for_model(ext_object)
        currency = get_choice(self.site, KPlansK.CHOICE_CURRENCY_CODE.value, code=currency_code)

        pay_entry, created = PayEntry.objects.get_or_create(
            site=account.site,
            account=account,
            ext_object_pk=ext_object.pk,
            ext_content_type=content_type,
            defaults={
                'amount': amount,
                'currency': currency,
                'paid_by': user,
                'type': ttype,
                'calc_amount': calc_amount,
            })

        if created:
            account.balance += calc_amount
            account.save(update_fields=['balance'])

        return pay_entry

    def credit_account(self, account, user, currency, amount, ext_object):
        return self._add_pay_entry(KPlansK.PAY_CREDIT.value, account, user, currency, amount, ext_object)

    def debit_account(self, account, user, currency, amount, ext_object):
        # This app does not support negative balance. We don't want to turn into an accounting library
        if account.balance >= amount:
            return self._add_pay_entry(KPlansK.PAY_DEBIT.value, account, user, currency, amount, ext_object)
