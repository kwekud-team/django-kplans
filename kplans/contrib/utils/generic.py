from django.template.defaultfilters import slugify
from kplans.models import Choice


def namefy(code, separators=None):
    separators = separators or []
    value = code
    for sep in separators:
        value = code.replace(sep, ' ')
    return value


def get_choice(site, ttype, name=None, code=None, **defaults):
    if not name and not code:
        raise Exception('name or code must be provided')

    name = name or namefy(code).title()
    code = code or slugify(name)

    defaults = defaults or {}
    return Choice.objects.get_or_create(
        site=site,
        type=ttype,
        code=code,
        defaults=dict(
            name=name,
            **defaults
        ))[0]
