import datetime
from django.utils import timezone

from kommons.utils.generic import decify
from kplans.models import Plan, Pricing, PlanPricing
from kplans.contrib.constants import KPlansK
from kplans.contrib.utils.generic import namefy, get_choice


class PlanUtils:

    def __init__(self, site):
        self.site = site

    def get_default_plan(self):
        plan = Plan.objects.filter(site=self.site, is_default=True).first()
        return plan or self.get_plan('default', is_default=True)

    def get_plan(self, code, name=None, **defaults):
        name = name or namefy(code)
        return Plan.objects.update_or_create(
            site=self.site,
            code=code,
            name=name,
            defaults=dict(
                is_visible=True,
                **defaults
            ))[0]

    def get_pricing(self, period=30, name=''):
        return Pricing.objects.update_or_create(
            site=self.site,
            period=period,
            defaults={
                'name': name,
            })[0]

    def get_plan_pricing(self, plan, pricing, price, currency_code, **defaults):
        currency = get_choice(self.site, KPlansK.CHOICE_CURRENCY_CODE.value, code=currency_code)
        return PlanPricing.objects.update_or_create(
            site=self.site,
            plan=plan,
            pricing=pricing,
            defaults=dict(
                price=decify(price),
                currency=currency,
                **defaults
            ))[0]

    def quick_plan_pricing(self, code, period, price, currency_code):
        plan = self.get_plan(code, is_free=price == 0)
        pricing = self.get_pricing(period)
        return self.get_plan_pricing(plan, pricing, price, currency_code)

    def get_expiry_date(self, plan_pricing):
        return (timezone.now() + datetime.timedelta(days=plan_pricing.pricing.period)).date()

    def get_plan_savings(self, plan_pricing):
        """
        This calculates savings based on the lowest pricing price. This is normally useful if you want to encourage
        users to pay for longer period, so you reduce price. eg: Monthly plan is $10, but Yearly plan is $110 instead
        of $120 ($10 * 12).
        """
        savings, percent = 0, 0
        base_pricing = plan_pricing.plan.planpricing_set.active().order_by('price').first()
        if base_pricing and base_pricing.pricing.period:
            unit_price = base_pricing.price / base_pricing.pricing.period

            base_rel_total = unit_price * plan_pricing.pricing.period

            savings = base_rel_total - plan_pricing.price
            percent = 0
            if base_rel_total and (base_pricing != plan_pricing):
                percent = (savings / base_rel_total) * 100

        return {
            'amount': savings,
            'percent': percent,
        }
