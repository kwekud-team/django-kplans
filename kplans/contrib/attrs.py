
class ChoiceAttr:
    admin_list_display = ['code', 'name', 'type', 'description', 'order', 'site']
    admin_list_filter = ['site', 'is_active', 'type']
    admin_search_fields = ['name', 'code']
    admin_list_editable = ['order']
    admin_form_fields = ['site', 'code', 'name', 'type', 'description', 'order', 'is_active']
    drf_serializer_fields = ['pk', 'guid', 'is_active', 'site', 'code', 'name', 'type', 'description', 'order']


class PlanAttr:
    admin_list_display = ['code', 'name', 'description', 'is_default', 'is_available', 'is_free', 'is_visible',
                          'is_recommended', 'use_custom_pricing', 'order', 'site']
    admin_list_filter = ['site', 'is_active', 'is_default', 'is_available', 'is_recommended', 'is_free', 'is_visible',]
    admin_search_fields = ['name', 'code']
    admin_list_editable = ['order',  'is_default', 'is_available', 'is_recommended', 'use_custom_pricing']
    admin_form_fields = ['site', 'code', 'name', 'description', 'is_default', 'is_available', 'is_free', 'is_visible',
                         'is_recommended', 'use_custom_pricing', 'extra', 'order', 'is_active']
    drf_serializer_fields = ['pk', 'guid', 'is_active', 'site', 'code', 'name', 'description', 'is_default',
                             'is_available', 'is_free', 'is_visible', 'is_recommended', 'use_custom_pricing', 'extra']


class PricingAttr:
    admin_list_display = ['name', 'period', 'order', 'site']
    admin_list_filter = ['site', 'is_active']
    admin_search_fields = ['name']
    admin_list_editable = ['order']
    admin_form_fields = ['site', 'name', 'period', 'order', 'is_active']
    drf_serializer_fields = ['pk', 'guid', 'is_active', 'site', 'name', 'period', 'order']


class FeatureAttr:
    admin_list_display = ['code', 'name', 'unit', 'description', 'category', 'is_boolean', 'order',
                          'site']
    admin_list_filter = ['site', 'is_active', 'category']
    admin_search_fields = ['name', 'code']
    admin_list_editable = ['order']
    admin_form_fields = ['site', 'code', 'name', 'unit', 'description', 'category', 'is_boolean', 'extra', 'order',
                         'is_active']
    drf_serializer_fields = ['pk', 'guid', 'is_active', 'site',  'code', 'name', 'unit', 'description', 'category',
                             'is_boolean', 'extra', 'order']


class PlanPricingAttr:
    admin_form_fields = ['pricing', 'currency', 'price', 'has_automatic_renewal', 'order', 'is_active']
    drf_serializer_fields = ['pk', 'guid', 'is_active', 'site', 'plan', 'pricing', 'price', 'has_automatic_renewal',
                             'order']


class PlanFeatureAttr:
    admin_form_fields = ['feature', 'value', 'has_access', 'highlighted', 'order', 'is_active']
    drf_serializer_fields = ['pk', 'guid', 'is_active', 'site', 'feature', 'value', 'has_access', 'order', 'note']


class AccountAttr:
    admin_list_display = ['user', 'plan', 'extended_date', 'status', 'is_valid', 'current_order', 'balance', 'site']
    admin_list_filter = ['site', 'is_active', 'extended_date', 'status', 'is_valid']
    admin_search_fields = ['plan__name']
    admin_form_fields = ['site', 'user', 'plan', 'extended_date', 'status', 'is_valid', 'current_order']
    drf_serializer_fields = ['pk', 'guid', 'is_active', 'site',  'user', 'plan', 'extended_date', 'status',
                             'is_valid', 'current_order']


class PayEntryAttr:
    admin_list_display = ['account', 'type', 'paid_by', 'currency', 'amount', 'ext_content_type', 'ext_content_object',
                          'site']
    admin_list_filter = ['site', 'is_active', 'type']
    admin_search_fields = ['type']
    drf_serializer_fields = ['pk', 'guid', 'is_active', 'site', 'account', 'type', 'paid_by', 'currency', 'amount',
                             'calc_amount', 'ext_object_pk', 'ext_content_type', 'site']
    
    
class OrderAttr:
    admin_list_display = ['account', 'plan_pricing', 'created_by', 'currency', 'amount', 'status', 'expiry_date',
                          'date_completed', 'date_validated', 'payment_ref', 'response_code', 'site']
    admin_list_filter = ['site', 'is_active', 'expiry_date', 'date_completed', 'date_validated', 'status']
    admin_search_fields = ['plan_pricing__pricing__name']
    drf_serializer_fields = ['pk', 'guid', 'is_active', 'site', 'account', 'plan_pricing', 'created_by',
                             'currency', 'amount', 'status', 'expiry_date', 'date_completed', 'date_validated',
                             'payment_ref', 'response_code', 'response_message']
