import uuid
from django.db import models

from kplans.managers import BaseManager


class BaseModel(models.Model):
    guid = models.UUIDField(unique=True)
    is_active = models.BooleanField(default=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    objects = BaseManager()

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.guid = self.guid or uuid.uuid4()
        super().save(*args, **kwargs)


class SiteBaseModel(BaseModel):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)

    class Meta:
        abstract = True
