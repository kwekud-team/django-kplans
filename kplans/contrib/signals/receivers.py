from django.dispatch import receiver
from kpay.models import Payment
from kpay.contrib.signals.dispatch import kpay_status_changed

from kplans.models import Order
from kplans.contrib.utils.account import AccountUtils


@receiver(kpay_status_changed)
def payment_status_changed(sender, instance, event, **kwargs):
    if sender == Payment:
        order = Order.objects.filter(guid=instance.external_ref).first()
        if order:
            account_utils = AccountUtils(order.site)

            if instance.date_validated:
                account_utils.credit_account(order.account, order.created_by, instance.currency, instance.total,
                                             instance)
                account_utils.pay_for_order(order, order.created_by)

            elif instance.status == 'rejected':
                account_utils.cancel_order(order, str(instance.guid), 'Payment rejected')
