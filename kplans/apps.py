from django.apps import AppConfig


class CustomConfig(AppConfig):
    name = 'kplans'

    def ready(self):
        from kplans.contrib.signals import receivers

