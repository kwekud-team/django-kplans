from django.contrib import admin

from kplans.models import Choice, Plan, Pricing, Feature, PlanFeature, PlanPricing, Account, PayEntry, Order
from kplans.forms import ChoiceAdminForm
from kplans.contrib.attrs import (ChoiceAttr, PlanAttr, PricingAttr, FeatureAttr, PlanPricingAttr, PlanFeatureAttr,
                                  AccountAttr, PayEntryAttr, OrderAttr)


@admin.register(Choice)
class ChoiceAdmin(admin.ModelAdmin):
    list_display = ChoiceAttr.admin_list_display
    list_filter = ChoiceAttr.admin_list_filter
    search_fields = ChoiceAttr.admin_search_fields
    list_editable = ChoiceAttr.admin_list_editable
    fields = ChoiceAttr.admin_form_fields
    form = ChoiceAdminForm


class PlanPriceInline(admin.TabularInline):
    model = PlanPricing
    extra = 1
    fields = PlanPricingAttr.admin_form_fields


class PlanFeatureInline(admin.TabularInline):
    model = PlanFeature
    extra = 1
    fields = PlanFeatureAttr.admin_form_fields


@admin.register(Plan)
class PlanAdmin(admin.ModelAdmin):
    list_display = PlanAttr.admin_list_display
    list_filter = PlanAttr.admin_list_filter
    search_fields = PlanAttr.admin_search_fields
    list_editable = PlanAttr.admin_list_editable
    fields = PlanAttr.admin_form_fields
    inlines = [PlanPriceInline, PlanFeatureInline]


@admin.register(Pricing)
class PricingAdmin(admin.ModelAdmin):
    list_display = PricingAttr.admin_list_display
    list_filter = PricingAttr.admin_list_filter
    search_fields = PricingAttr.admin_search_fields
    list_editable = PricingAttr.admin_list_editable
    fields = PricingAttr.admin_form_fields


@admin.register(Feature)
class FeatureAdmin(admin.ModelAdmin):
    list_display = FeatureAttr.admin_list_display
    list_filter = FeatureAttr.admin_list_filter
    search_fields = FeatureAttr.admin_search_fields
    list_editable = FeatureAttr.admin_list_editable
    fields = FeatureAttr.admin_form_fields


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = AccountAttr.admin_list_display
    list_filter = AccountAttr.admin_list_filter
    search_fields = AccountAttr.admin_search_fields
    fields = AccountAttr.admin_form_fields


@admin.register(PayEntry)
class PayEntryAdmin(admin.ModelAdmin):
    list_display = PayEntryAttr.admin_list_display
    list_filter = PayEntryAttr.admin_list_filter
    search_fields = PayEntryAttr.admin_search_fields


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = OrderAttr.admin_list_display
    list_filter = OrderAttr.admin_list_filter
    search_fields = OrderAttr.admin_search_fields
